const mongoose = require('mongoose');
const config = require("./config");
const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');


mongoose.connect(config.db.url + "/" + config.db.name);

const db = mongoose.connection;

const collections = ['users', 'categories', 'products'];

db.once("open", async () => {

  collections.forEach(async collectionName => {
    try {
      await db.dropCollection(collectionName);
    } catch (e) {
      console.log(`Collection ${collectionName} did not exist in DB`);
    }
  });

  const [user1, user2] = await User.create({
    username: 'superman',
    password: '12345',
    name: 'John Dow',
    phone: '+996 777 777-777'
  }, {
    username: 'batman',
    password: '123',
    name: 'Jack Black',
    phone: '+996 444 444-444'
  });

  const [compCategory, tabletCategory, phoneCategory] = await Category.create({
    title: 'Computers'
  }, {
    title: 'Tablets'
  }, {
    title: 'Phones'
  });

   await Product.create({
    title: 'Asus - VivoBook Max X541NA',
    price: 279.99,
    description: "ASUS VivoBook Max X541NA Laptop: Get work done with this durable Asus notebook PC. The Intel Pentium N4200 processor and 4GB of RAM support multitasking for business or personal tasks, and the 15.6-inch HD screen delivers sharp clarity for editing or viewing pictures. This Asus notebook PC is engineered with SonicMaster technology for a cinematic audio experience.",
    category: compCategory._id,
    seller: user1._id,
    image: 'asus-vivobook.jpg'
  }, {
    title: 'Apple - iPad (5th generation)',
    price: 408.99,
    description: "Powerful, portable, and personal with a gorgeous 9.7-inch Retina display¹ in a thin, durable aluminum design that weighs just a pound. iPad puts incredible capabilities in your hands with a powerful A9 chip, 8MP camera, FaceTime HD camera, Touch ID, Apple Pay, Wi-Fi, all-day battery life, ² and over a million apps on the App Store.",
    category: tabletCategory._id,
    seller: user1._id,
    image: 'Apple-iPad.jpg'
  },{
     title: 'Samsung - Galaxy Tab E Lite ',
     price: 79.99,
     description: 'Put the power of the Internet in your pocket with the Samsung Galaxy Tab E Lite. A bright 7-inch touchscreen lets you browse websites or watch videos in virtually any light. With built-in parental controls, the Samsung Galaxy Tab E Lite helps you monitor your children\'s usage habits as they explore apps and games.',
     category: tabletCategory._id,
     seller: user2._id,
     image: 'Samsung - galaxy Tab.jpg'
   }, {
     title: 'Samsung - Galaxy S9',
     price: 699.99,
     description: 'Choose a carrier on your own time and take breathtaking high-quality shots with this unlocked Samsung Galaxy S9 in black. Pro Mode manual adjustments deliver professional photos frame by frame, while the dual aperture lens shoots clear in any lighting. Easy access from Iris Scanner and Facial Recognition opens your phone with a look. With included Augmented Reality that does everything from translating text to turning users into emojis, this unlocked Samsung Galaxy S9 brings imagination to life.',
     category: phoneCategory._id,
     seller: user2._id,
     image: 'Samsung S9.jpg'
   }, {
     title: 'Apple iPhone 6 4G LTE with 32GB',
     price: 199.99,
     description: 'Keep your contacts and important documents close at hand with this Apple iPhone 6, which connects with iCloud to share documents and information with your computer. The slim design of this phone doesn\'t skimp on features while being heavy on style. Internal storage of 32GB on this Apple iPhone 6 lets you hang on multiple playlists.',
     category: phoneCategory._id,
     seller: user1._id,
     image: 'Apple iPhone 6.jpg'
   });

  db.close();
});
const express = require("express");
const multer = require("multer");
const path = require("path");
const nanoid = require("nanoid");
const auth = require('../middleware/auth');
const Product = require("../models/Product");

const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const router = express.Router();

const createRouter = () => {

  router.get("/", (req, res) => {
    Product.find()
      .populate("category")
      .populate('seller')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  router.post("/", [auth, upload.single("image")], (req, res) => {
    const productData = req.body;

    if (req.file) {
      productData.image = req.file.filename;
    } else {
      productData.image = null;
    }

    productData.seller = req.user._id;

    const product = new Product(productData);

    product
      .save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    Product.findById(id)
    .populate("category")
    .populate('seller')
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  router.get("/categories/:id", (req, res) => {
    const id = req.params.id;
    Product.find({category: id})
    .then(result => {
      if (result) res.send(result);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
  });


  router.delete('/:id', auth, (req, res) => {
    const id = req.params.id;
    Product.findOne({_id: id}).then(result => {
      if (!req.user._id.equals(result.seller)) {
        res.sendStatus(403);
      } else {
        Product.findByIdAndRemove(id, (err, doc) => {
          err ? res.status(404) : res.send({ id: doc._id})
        }).catch(err => res.status(500).send(err));
      }
    })
  });

  return router;
};

module.exports = createRouter;
